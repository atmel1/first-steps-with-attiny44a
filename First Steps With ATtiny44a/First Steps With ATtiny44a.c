/*
 * First_Steps_With_ATtiny44a.c
 *
 * Created: 5/29/2015 16:08:35
 *  Author: Brandy
 */ 


#include <avr/io.h>
#define F_CPU 20000000
#include <util/delay.h>

int main(void)
{
	DDRA |= (1<<DDA0) | (1<<DDA1) | (1<<DDA2) | (1<<DDA3) | (1<<DDA4) | (1<<DDA5) | (1<<DDA6) | (1<<DDA7);
	PORTB = 0;
	/*
		DDxn PORTxn PUD (in MCUCR) I/O   Pull-up  Comment
		 0     0         X        Input    No     Tri-state (Hi-Z)
		 0     1         0        Input   Yes     Pxn will source current if ext. pulled low
		 0     1         1        Input    No     Tri-state (Hi-Z)
		 1     0         X        Output   No     Output Low (Sink)
		 1     1         X        Output   No     Output High (Source)
	*/
    while(1)
    {
		PINA |= (1 << PINA7)|(1 << PINA6)|(1 << PINA5)|(1 << PINA4)|(1 << PINA3)|(1 << PINA2)|(1 << PINA1)|(1 << PINA0);
		/*
			Bit            7     6     5     4     3     2     1     0
			0x19 (0x39)  PINA7 PINA6 PINA5 PINA4 PINA3 PINA2 PINA1 PINA0   PINA
			Read/Write    R/W   R/W   R/W   R/W   R/W   R/W   R/W   R/W
			Initial Value N/A   N/A   N/A   N/A   N/A   N/A   N/A   N/A
		*/
		_delay_ms(1000);
		//_delay_ms(10);
        //TODO:: Please write your application code 
    }
}